from roslina import Roslina
class WilczaJagoda(Roslina):
    """description of class"""
    def __init__(self,xx,yy,pochod):
        super(WilczaJagoda,self).__init__(xx,yy,103,0,0,"Wilcza Jagoda",1,pochod)

    def rysowanie(self):
        return '#'

    def akcja(self):
        wiekk=self.getWiek()
        if (wiekk >= 0 and wiekk <= 35):
            super(WilczaJagoda,self).akcja()
        self.setWiek(wiekk+3)

    def kolizja(self,kolizujacy):
        import komunikaty
        pochodzenie=self.getPochodzenie()
        pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"R",komunikaty.Komunikaty.ZUM.value)
        return komunikaty.Komunikaty.ZUM.value