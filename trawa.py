from roslina import Roslina
class Trawa(Roslina):
    """description of class"""
    def __init__(self,xx,yy,pochod):
        super(Trawa,self).__init__(xx,yy,100,0,0,"Trawa",1,pochod)

    def rysowanie(self):
        return 'm'

    def akcja(self):
        wiekk=self.getWiek()
        if (wiekk >= 0 and wiekk <= 35):
            super(Trawa,self).akcja()
        self.setWiek(wiekk+3)