from zwierze import Zwierze
class Lis(Zwierze):
    """description of class"""
    def __init__(self,xx,yy,pochod):
        super(Lis,self).__init__(xx,yy,203,3,7,"Lis",1,pochod)

    def rysowanie(self):
        return 'F'

    def akcja(self):
        import random
        pochodzenie=self.getPochodzenie()
        max_x = pochodzenie.getmaxX()
        max_y = pochodzenie.getmaxY()
        organizmy = pochodzenie.getOrg()
        size_active = pochodzenie.getSizeOfHeap()
        kolejnosc = pochodzenie.getKol()
        ruch=random.randint(0, 3)
        xpom=self.getX()
        ypom=self.getY()
        import swiat
        import komunikaty
        if (ruch==0):
            if (ypom > 0):
                if ((organizmy[xpom + (ypom - 1)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x)
                    self.setY(ypom-1)
                else:
                    zdarzenie = organizmy[xpom + (ypom - 1)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom - 1)*max_x)
                        self.setY(ypom-1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
                #                del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + (ypom - 1)*max_x] = None
                 #           del organizmy[xpom + (ypom - 1)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom - 1)*max_x);
                        self.setY(ypom-1);

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
                        KomunikatBox=pochodzenie.ui.getKomunikaty()
                        kom=self.getNazwa()+" na pozycji "+str(self.getX())+" "+str(self.getY())+" chcial zaatakowac "+organizmy[xpom + (ypom - 1)*max_x].getNazwa()+ " na pozycji "+str(organizmy[xpom + (ypom - 1)*max_x].getX())+" "+ str(organizmy[xpom + (ypom - 1)*max_x].getY())+" ale zrezygnowal"
                        KomunikatBox.append(kom)

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]):
                     #           del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + (ypom - 1)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
              #              del organizmy[xpom + (ypom - 1)*max_x]
            #                del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (ruch==1):
            if (ypom <max_y-1):
                if ((organizmy[xpom + (ypom + 1)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x)
                    self.setY(ypom+1)
                else:
                    zdarzenie = organizmy[xpom + (ypom + 1)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom + 1)*max_x)
                        self.setY(ypom+1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
           #                     del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + (ypom + 1)*max_x] = None
                #            del organizmy[xpom + (ypom + 1)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom + 1)*max_x)
                        self.setY(ypom+1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
                        KomunikatBox=pochodzenie.ui.getKomunikaty()
                        kom=self.getNazwa()+" na pozycji "+str(self.getX())+" "+str(self.getY())+" chcial zaatakowac "+organizmy[xpom + (ypom + 1)*max_x].getNazwa()+ " na pozycji "+str(organizmy[xpom + (ypom + 1)*max_x].getX())+" "+ str(organizmy[xpom + (ypom + 1)*max_x].getY())+" ale zrezygnowal"
                        KomunikatBox.append(kom)

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]):
           #                     del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + (ypom + 1)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
            #                del organizmy[xpom + (ypom + 1)*max_x]
           #                 del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (ruch==2):
            if (xpom >0):
                if ((organizmy[xpom - 1 + (ypom)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x)
                    self.setX(xpom-1)
                else:
                    zdarzenie = organizmy[xpom - 1 + (ypom)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom - 1 + (ypom)*max_x)
                        self.setX(xpom-1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom - 1 + (ypom)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
                #                del kolejnosc[i]
                                break
                                              
                        organizmy[xpom - 1 + (ypom)*max_x] = None;
               #             del organizmy[xpom - 1 + (ypom)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom - 1 + (ypom)*max_x);
                        self.setX(xpom-1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
                        KomunikatBox=pochodzenie.ui.getKomunikaty()
                        kom=self.getNazwa()+" na pozycji "+str(self.getX())+" "+str(self.getY())+" chcial zaatakowac "+organizmy[xpom - 1 + (ypom)*max_x].getNazwa()+ " na pozycji "+str(organizmy[xpom - 1 + (ypom)*max_x].getX())+" "+ str(organizmy[xpom - 1 + (ypom)*max_x].getY())+" ale zrezygnowal"
                        KomunikatBox.append(kom)

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom - 1 + (ypom)*max_x] == kolejnosc[i]):
                 #               del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom - 1 + (ypom)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
             #               del organizmy[xpom - 1 + (ypom)*max_x]
            #                del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (ruch==3):
            if (xpom <max_x-1):
                if ((organizmy[xpom + 1 + (ypom)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x)
                    self.setX(xpom+1)
                else:
                    zdarzenie = organizmy[xpom + 1 + (ypom)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + 1 + (ypom)*max_x)
                        self.setX(xpom+1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + 1 + (ypom)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
          #                      del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + 1 + (ypom)*max_x] = None
                    #        del organizmy[xpom + 1 + (ypom)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + 1 + (ypom)*max_x);
                        self.setX(xpom+1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
                        KomunikatBox=pochodzenie.ui.getKomunikaty()
                        kom=self.getNazwa()+" na pozycji "+str(self.getX())+" "+str(self.getY())+" chcial zaatakowac "+organizmy[xpom + 1 + (ypom)*max_x].getNazwa()+ " na pozycji "+str(organizmy[xpom + 1 + (ypom)*max_x].getX())+" "+ str(organizmy[xpom + 1 + (ypom)*max_x].getY())+" ale zrezygnowal"
                        KomunikatBox.append(kom)

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + 1 + (ypom)*max_x] == kolejnosc[i]):
                 #               del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + 1 + (ypom)*max_x] = None;
                        organizmy[xpom + (ypom)*max_x] = None;
             #               del organizmy[xpom + 1 + (ypom)*max_x]
             #               del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)
        if (self.getZmeczony()>0):
            self.setZmeczony(self.getZmeczony()-1)
        self.setWiek(self.getWiek()+1)

