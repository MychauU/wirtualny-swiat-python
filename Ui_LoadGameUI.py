# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Informatyka\PycharmProjects\Wirtualnyswiat\LoadGameUI.ui'
#
# Created by: PyQt5 UI code generator 5.4.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_FormLoad(QtWidgets.QWidget):
    def __init__(self,pochod):
        self.pochodzenie=pochod
        super(Ui_FormLoad, self).__init__()

    def closeEvent(self, event):
        self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
        self.pochodzenie.setBlokada(False)

    def setupUi(self, FormLoad):
        FormLoad.setObjectName("FormLoad")
        FormLoad.resize(262, 163)
        FormLoad.setMinimumSize(QtCore.QSize(262, 163))
        FormLoad.setMaximumSize(QtCore.QSize(262, 163))
        self.wczytajButton = QtWidgets.QPushButton(FormLoad)
        self.wczytajButton.setGeometry(QtCore.QRect(180, 130, 75, 23))
        self.wczytajButton.setObjectName("wczytajButton")

        self.wczytajButton.clicked.connect(self.tryLoadGame)

        self.formLayoutWidget = QtWidgets.QWidget(FormLoad)
        self.formLayoutWidget.setGeometry(QtCore.QRect(0, 20, 261, 101))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setFrameShape(QtWidgets.QFrame.Box)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.label)
        self.lineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit.setObjectName("lineEdit")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.lineEdit)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.formLayout.setItem(2, QtWidgets.QFormLayout.FieldRole, spacerItem)

        self.retranslateUi(FormLoad)
        QtCore.QMetaObject.connectSlotsByName(FormLoad)

    def retranslateUi(self, FormLoad):
        _translate = QtCore.QCoreApplication.translate
        FormLoad.setWindowTitle(_translate("FormLoad", "Wczytaj Gre"))
        self.wczytajButton.setText(_translate("FormLoad", "Wczytaj"))
        self.label.setText(_translate("FormLoad", "Podaj nazwę pliku wczytywanej gry:"))

    def tryLoadGame(self):
        rawString=self.lineEdit.text()
        if (rawString != "" ):
            CzyString=self.isString(rawString)
            if (CzyString==True):
                nazwa=str(rawString)
                self.pochodzenie.ui.wczytajGre(nazwa)
            else:
                KomunikatBox=self.pochodzenie.ui.getKomunikaty()
                kom="NAZWA PLIKU  NIE JEST POPRAWNA LEKSYKOGRAFICZNIE"
                KomunikatBox.append(kom)
        else:
            KomunikatBox=self.pochodzenie.ui.getKomunikaty()
            kom="POLE NAZWA PLIKU NIE JEST UZUPELNIONE"
            KomunikatBox.append(kom)

    @staticmethod
    def isString(x):
        try:
            str(x)
            return True
        except ValueError:
            return False



