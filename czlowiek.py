from zwierze import Zwierze
class Czlowiek(Zwierze):
    """description of class"""
    def __init__(self,xx,yy,pochod):
        super(Czlowiek,self).__init__(xx,yy,155056 % 5,5,4,"Czlowiek",1,pochod)
        self.__moc=0
        self.__nietykalnosc=False
        self.__niesmiertelonsc=False
        self.__szybkosc=False
        self.__powerbuff=0
        self.__cooldown=0
        self.__ruch = ""

    def getRuch(self):
        return self.__ruch

    def setRuch(self,x):
        self.__ruch = x

    def rysowanie(self):
        return 'H'

    def castSkill(self):
        if (self.__cooldown == 0):
            if (self.getId() == 1):
                self.risePower()
        else:
            pochodzenie=self.getPochodzenie()
            KomunikatBox=pochodzenie.ui.getKomunikaty()
            kom="TWOJE SKILLE SA JESZCZE NA COOLDOWNIE!"
            KomunikatBox.append(kom)

    def risePower(self):
        self.__cooldown = 10
        self.setSila(self.getSila()+ 5)
        self.__powerbuff = 5
        pochodzenie=self.getPochodzenie()
        KomunikatBox=pochodzenie.ui.getKomunikaty()
        kom="UMIEJTNOSC 'MAGICZNY ELIKSIR' WLACZONA!"
        KomunikatBox.append(kom)

    def akcja(self):
        import random
        pochodzenie=self.getPochodzenie()
        if (self.__powerbuff!=0):
            self.__powerbuff-=1
            self.setSila(self.getSila()-1)
        if (self.__cooldown!=0):
            self.__cooldown-=1
        max_x = pochodzenie.getmaxX()
        max_y = pochodzenie.getmaxY()
        organizmy = pochodzenie.getOrg()
        size_active = pochodzenie.getSizeOfHeap()
        kolejnosc = pochodzenie.getKol()
        ruch=self.__ruch
        xpom=self.getX()
        ypom=self.getY()
        import swiat
        import komunikaty
        if (self.__ruch == "SKILL"):
            self.castSkill()
        elif (self.__ruch=="KEY UP"):
            if (ypom > 0):
                if ((organizmy[xpom + (ypom - 1)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x)
                    self.setY(ypom-1)
                else:
                    zdarzenie = organizmy[xpom + (ypom - 1)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom - 1)*max_x)
                        self.setY(ypom-1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
                #                del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + (ypom - 1)*max_x] = None
                 #           del organizmy[xpom + (ypom - 1)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom - 1)*max_x);
                        self.setY(ypom-1);

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
    #                    del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]):
                     #           del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + (ypom - 1)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
              #              del organizmy[xpom + (ypom - 1)*max_x]
            #                del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (self.__ruch=="KEY DOWN"):
            if (ypom <max_y-1):
                if ((organizmy[xpom + (ypom + 1)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x)
                    self.setY(ypom+1)
                else:
                    zdarzenie = organizmy[xpom + (ypom + 1)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom + 1)*max_x)
                        self.setY(ypom+1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
           #                     del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + (ypom + 1)*max_x] = None
                #            del organizmy[xpom + (ypom + 1)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom + 1)*max_x)
                        self.setY(ypom+1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
             #           del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]):
           #                     del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + (ypom + 1)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
            #                del organizmy[xpom + (ypom + 1)*max_x]
           #                 del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (self.__ruch=="KEY LEFT"):
            if (xpom >0):
                if ((organizmy[xpom - 1 + (ypom)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x)
                    self.setX(xpom-1)
                else:
                    zdarzenie = organizmy[xpom - 1 + (ypom)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom - 1 + (ypom)*max_x)
                        self.setX(xpom-1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom - 1 + (ypom)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
                #                del kolejnosc[i]
                                break
                                              
                        organizmy[xpom - 1 + (ypom)*max_x] = None;
               #             del organizmy[xpom - 1 + (ypom)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom - 1 + (ypom)*max_x);
                        self.setX(xpom-1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
            #            del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom - 1 + (ypom)*max_x] == kolejnosc[i]):
                 #               del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom - 1 + (ypom)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
             #               del organizmy[xpom - 1 + (ypom)*max_x]
            #                del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (self.__ruch=="KEY RIGHT"):
            if (xpom <max_x-1):
                if ((organizmy[xpom + 1 + (ypom)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x)
                    self.setX(xpom+1)
                else:
                    zdarzenie = organizmy[xpom + 1 + (ypom)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + 1 + (ypom)*max_x)
                        self.setX(xpom+1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + 1 + (ypom)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
          #                      del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + 1 + (ypom)*max_x] = None
                    #        del organizmy[xpom + 1 + (ypom)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + 1 + (ypom)*max_x);
                        self.setX(xpom+1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
             #           del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + 1 + (ypom)*max_x] == kolejnosc[i]):
                 #               del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + 1 + (ypom)*max_x] = None;
                        organizmy[xpom + (ypom)*max_x] = None;
             #               del organizmy[xpom + 1 + (ypom)*max_x]
             #               del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)
        if (self.getZmeczony()>0):
            self.setZmeczony(self.getZmeczony()-1)
        self.setWiek(self.getWiek()+1)         
