from roslina import Roslina
class Guarana(Roslina):
    """description of class"""
    def __init__(self,xx,yy,pochod):
        super(Guarana,self).__init__(xx,yy,102,0,0,"Guarana",1,pochod)

    def rysowanie(self):
        return '{'

    def akcja(self):
        wiekk=self.getWiek()
        if (wiekk >= 0 and wiekk <= 35):
            super(Guarana,self).akcja()
        self.setWiek(wiekk+3)

    def kolizja(self,kolizujacy):
        import komunikaty
        kolizujacy.setSila(kolizujacy.getSila()+3)
        pochodzenie=self.getPochodzenie()
        pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"R",komunikaty.Komunikaty.ZABIJ.value)
        return komunikaty.Komunikaty.ZABIJ.value