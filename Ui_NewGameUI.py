# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Informatyka\PycharmProjects\Wirtualnyswiat\NewGameUI.ui'
#
# Created by: PyQt5 UI code generator 5.4.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_NowaGra(QtWidgets.QWidget):
    def __init__(self,pochod):
        self.pochodzenie=pochod
        super(Ui_NowaGra, self).__init__()

    def closeEvent(self, event):
        self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
        self.pochodzenie.setBlokada(False)

    def setupUi(self, NowaGra):
        NowaGra.setObjectName("NowaGra")
        NowaGra.resize(232, 167)
        NowaGra.setMinimumSize(QtCore.QSize(232, 167))
        NowaGra.setMaximumSize(QtCore.QSize(232, 167))
        self.formLayoutWidget = QtWidgets.QWidget(NowaGra)
        self.formLayoutWidget.setGeometry(QtCore.QRect(0, 0, 231, 101))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.lineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit.setText("10")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lineEdit)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.lineEdit_2.setText("10")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.lineEdit_2)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.formLayout.setItem(1, QtWidgets.QFormLayout.FieldRole, spacerItem)
        self.label_3 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.pushButton = QtWidgets.QPushButton(NowaGra)
        self.pushButton.setGeometry(QtCore.QRect(150, 140, 75, 23))
        self.pushButton.setObjectName("pushButton")

        self.pushButton.clicked.connect(self.tryNewGame)

        self.retranslateUi(NowaGra)
        QtCore.QMetaObject.connectSlotsByName(NowaGra)

    def retranslateUi(self, NowaGra):
        _translate = QtCore.QCoreApplication.translate
        NowaGra.setWindowTitle(_translate("NowaGra", "Stworz Nową grę"))
        self.label.setText(_translate("NowaGra", "Podaj X"))
        self.label_3.setText(_translate("NowaGra", "Podaj Y"))
        self.pushButton.setText(_translate("NowaGra", "Stwórz"))

    def tryNewGame(self):
        newmax_x=self.lineEdit.text()
        newmax_y=self.lineEdit_2.text()
        if (newmax_x != "" and newmax_y !=""):
            Czyliczba=self.isNumber(newmax_x,newmax_y)
            if (Czyliczba==True):
                intx=int(newmax_x)
                inty=int(newmax_y)
                if (intx>=7 and intx<=30 and inty>=7 and intx<=30):
                    self.pochodzenie.ui.stworzNowaGre(intx,inty)
                    KomunikatBox=self.pochodzenie.ui.getKomunikaty()
                    KomunikatBox.clear()
                    kom="STWORZYLES NOWA GRE"
                    self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
                    self.pochodzenie.ui.NowaGra.setVisible(False)
                    self.pochodzenie.setBlokada(False)
                    KomunikatBox.append(kom)
                else:
                    KomunikatBox=self.pochodzenie.ui.getKomunikaty()
                    kom="PODALES ZLE WYMIARY: Poprawne to min=>7 , max<=30"
                    KomunikatBox.append(kom)
            else:
                KomunikatBox=self.pochodzenie.ui.getKomunikaty()
                kom="PODANE PARAMETRY NIE SA LICZBAMI"
                KomunikatBox.append(kom)
        else:
            KomunikatBox=self.pochodzenie.ui.getKomunikaty()
            kom="POLA DO WYMIAROW SA PUSTE!"
            KomunikatBox.append(kom)



    @staticmethod
    def isNumber(x,y):
        try:
            int(x)
            int(y)
            return True
        except ValueError:
            return False

