from PyQt5 import QtCore, QtGui, QtWidgets

class Button_X_Y(QtWidgets.QPushButton):
    def __init__(self,xx,yy,pochod):
        self.pochodzenie=pochod
        super(Button_X_Y, self).__init__()
        self.x=xx
        self.y=yy

    def mouseReleaseEvent(self, event):
        self.pochodzenie.ui.tryNewOrganizm(self.x,self.y)