# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Informatyka\PycharmProjects\Wirtualnyswiat\MainGUI.ui'
#
# Created by: PyQt5 UI code generator 5.4.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
class MyMainWindow(QtWidgets.QMainWindow):

    def __init__(self,pochod):
        self.pochodzenie=pochod
        super(MyMainWindow, self).__init__()
    def keyPressEvent(self, event):
        blokada=self.pochodzenie.getBlokada()
        if (blokada==False):
            czlowiek=self.pochodzenie.seekHuman()
            if (czlowiek is not None):
                key = event.key()
                if key == QtCore.Qt.Key_Left:
                    insertkey="KEY LEFT"
                elif key == QtCore.Qt.Key_Right:
                    insertkey="KEY RIGHT"
                elif key == QtCore.Qt.Key_Up:
                    insertkey="KEY UP"
                elif key == QtCore.Qt.Key_Down:
                    insertkey="KEY DOWN"
                elif key == QtCore.Qt.Key_Right:
                    insertkey="KEY RIGHT"
                elif key == QtCore.Qt.Key_P:
                    insertkey="SKILL"
                else:
                    insertkey=False
                if (insertkey is not False):
                    czlowiek.setRuch(insertkey)
                    self.pochodzenie.wykonajTure()
            else:
                self.pochodzenie.setBlokada(True)
                #napis ze koniec gry



class Ui_WirtualnySwiat(object):
    def __init__(self, pochod,WT):
        self.__pochodzenie=pochod
        self.NowaGra = QtWidgets.QWidget()
        self.ZapiszGra = QtWidgets.QWidget()
        self.WczytajGra = QtWidgets.QWidget()
        self.OrganizmGra = QtWidgets.QWidget()
        self.Komunikaty={}
        self.WirutalnySwiat = QtWidgets.QWidget(WT)
        import Ui_NewGameUI
        self.uiNG = Ui_NewGameUI.Ui_NowaGra(self.__pochodzenie)
        self.uiNG.setupUi(self.NowaGra)
        import Ui_SaveGameUI
        self.uiSG = Ui_SaveGameUI.Ui_FormSave(self.__pochodzenie)
        self.uiSG.setupUi(self.ZapiszGra)
        import Ui_LoadGameUI
        self.uiLG = Ui_LoadGameUI.Ui_FormLoad(self.__pochodzenie)
        self.uiLG.setupUi(self.WczytajGra)
        import Ui_NewOrganizm
        self.uiNO = Ui_NewOrganizm.Ui_NewOrganizmForm(self.__pochodzenie)
        self.uiNO.setupUi(self.OrganizmGra)

    def getKomunikaty(self):
        return self.Komunikaty

    def setupUi(self, WirtualnySwiat):
        WirtualnySwiat.setObjectName("WirtualnySwiat")
        WirtualnySwiat.resize(800, 600)
        WirtualnySwiat.setMinimumSize(QtCore.QSize(800, 680))
        WirtualnySwiat.setMaximumSize(QtCore.QSize(800, 680))
       # self.WirutalnySwiat = QtWidgets.QWidget(WirtualnySwiat)
        self.WirutalnySwiat.setEnabled(True)
        self.WirutalnySwiat.setMinimumSize(QtCore.QSize(800, 680))
        self.WirutalnySwiat.setMaximumSize(QtCore.QSize(800, 680))
        self.WirutalnySwiat.setObjectName("WirutalnySwiat")

        self.WirutalnySwiat.grabKeyboard()

        self.horizontalLayoutWidget = QtWidgets.QWidget(self.WirutalnySwiat)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 800, 650))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.line_2 = QtWidgets.QFrame(self.horizontalLayoutWidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout.addWidget(self.line_2)
        self.label = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.label.setObjectName("label")

        self.label.clicked.connect(self.odbierzFokus)

        self.verticalLayout.addWidget(self.label)
        self.line = QtWidgets.QFrame(self.horizontalLayoutWidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.frame = QtWidgets.QFrame(self.horizontalLayoutWidget)
        self.sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        self.sizePolicy.setHorizontalStretch(0)
        self.sizePolicy.setVerticalStretch(0)
        self.sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(self.sizePolicy)
        self.frame.setMinimumSize(QtCore.QSize(800, 600))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.frame.setStyleSheet("border: 0px solid #222222;")
        self.layoutWidget_2 = QtWidgets.QWidget(self.frame)
        self.layoutWidget_2.setGeometry(QtCore.QRect(0, 0, 800, 400))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.GameField = QtWidgets.QGridLayout(self.layoutWidget_2)
        self.GameField.setSpacing(0)
        self.GameField.setContentsMargins(0, 0, 0, 0)
        self.GameField.setObjectName("GameField")

        self.zaladujNowaMape()

        self.verticalLayout.addWidget(self.frame)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.line_3 = QtWidgets.QFrame(self.horizontalLayoutWidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout.addWidget(self.line_3)
        self.labelkom = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.labelkom.setObjectName("labelkom")
        self.verticalLayout.addWidget(self.labelkom)
        self.line_4 = QtWidgets.QFrame(self.horizontalLayoutWidget)
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.verticalLayout.addWidget(self.line_4)

        self.Komunikaty = QtWidgets.QTextBrowser(self.horizontalLayoutWidget)
        self.Komunikaty.setMaximumSize(800,150)
        self.Komunikaty.setMinimumSize(800,150)
        self.Komunikaty.setObjectName("Komunikaty")
        self.verticalLayout.addWidget(self.Komunikaty)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.horizontalLayout.addLayout(self.verticalLayout)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.horizontalLayout.addItem(spacerItem3)
        WirtualnySwiat.setCentralWidget(self.WirutalnySwiat)
        self.menuBar = QtWidgets.QMenuBar(WirtualnySwiat)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 700, 18))
        self.menuBar.setObjectName("menuBar")
        self.menuMenu = QtWidgets.QMenu(self.menuBar)
        self.menuMenu.setObjectName("menuMenu")
        self.menuPomoc = QtWidgets.QMenu(self.menuBar)
        self.menuPomoc.setObjectName("menuPomoc")
        WirtualnySwiat.setMenuBar(self.menuBar)
        self.actionNew_Game = QtWidgets.QAction(WirtualnySwiat)
        self.actionNew_Game.setObjectName("actionNew_Game")

        self.actionNew_Game.triggered.connect(self.stworzNowaGreAction)

        self.actionSave_Game = QtWidgets.QAction(WirtualnySwiat)
        self.actionSave_Game.setObjectName("actionSave_Game")

        self.actionSave_Game.triggered.connect(self.zapiszGreAction)

        self.actionLoad_Game = QtWidgets.QAction(WirtualnySwiat)
        self.actionLoad_Game.setObjectName("actionLoad_Game")

        self.actionLoad_Game.triggered.connect(self.wczytajGreAction)

        self.actionInstrukcja = QtWidgets.QAction(WirtualnySwiat)
        self.actionInstrukcja.setObjectName("actionInstrukcja")
        self.actionO_programie = QtWidgets.QAction(WirtualnySwiat)
        self.actionO_programie.setObjectName("actionO_programie")
        self.menuMenu.addAction(self.actionNew_Game)
        self.menuMenu.addAction(self.actionSave_Game)
        self.menuMenu.addAction(self.actionLoad_Game)
        self.menuPomoc.addAction(self.actionInstrukcja)
        self.menuPomoc.addAction(self.actionO_programie)
        self.menuBar.addAction(self.menuMenu.menuAction())
        self.menuBar.addAction(self.menuPomoc.menuAction())

        self.retranslateUi(WirtualnySwiat)
        QtCore.QMetaObject.connectSlotsByName(WirtualnySwiat)



    def retranslateUi(self, WirtualnySwiat):
        _translate = QtCore.QCoreApplication.translate
        WirtualnySwiat.setWindowTitle(_translate("WirtualnySwiat", "Wirtualny Świat"))
        self.label.setText(_translate("WirtualnySwiat", "Okno Gry\nNaciśnij tutaj gdy stracisz fokus"))
        self.labelkom.setText(_translate("WirtualnySwiat", "<html><head/><body><p align=\"center\">Komunikaty</p></body></html>"))
        self.menuMenu.setTitle(_translate("WirtualnySwiat", "Menu"))
        self.menuPomoc.setTitle(_translate("WirtualnySwiat", "Pomoc"))
        self.actionNew_Game.setText(_translate("WirtualnySwiat", "New Game"))
        self.actionSave_Game.setText(_translate("WirtualnySwiat", "Save Game"))
        self.actionLoad_Game.setText(_translate("WirtualnySwiat", "Load Game"))
        self.actionInstrukcja.setText(_translate("WirtualnySwiat", "Instrukcja"))
        self.actionO_programie.setText(_translate("WirtualnySwiat", "O programie"))

    def odbierzFokus(self):
        self.WirutalnySwiat.grabKeyboard()
        self.__pochodzenie.setBlokada(False)

    def zapiszGreAction(self):
        self.__pochodzenie.ui.WirutalnySwiat.releaseKeyboard()
        self.__pochodzenie.setBlokada(True)
        self.ZapiszGra.setVisible(True)

    def zapiszGre(self,nazwa):
        max_x=self.__pochodzenie.getmaxX()
        max_y=self.__pochodzenie.getmaxY()
        organizmy=self.__pochodzenie.getOrg()
        plik = open(nazwa, 'w')
        plik.write(str(max_x)+" "+str(max_y)+"\n")
        for i in range(0, max_x*max_y):
                if (organizmy[i] is not None):
                    plik.write(
                        str(organizmy[i].getId())+" "+str(organizmy[i].getX())+" "+str(organizmy[i].getY())+" "+str(organizmy[i].getSila())+" "+str(organizmy[i].getWiek())+"\n")
        plik.close()

    def wczytajGreAction(self):
        self.__pochodzenie.ui.WirutalnySwiat.releaseKeyboard()
        self.__pochodzenie.setBlokada(True)
        self.WczytajGra.setVisible(True)

    def wczytajGre(self,nazwa):
        try:
            import komunikaty
            import trawa
            import guarana
            import mlecz
            import wilczajagoda
            import wilk
            import owca
            import zolw
            import lis
            import antylopa
            import czlowiek
            text = open(nazwa)
            proba=[None]*5
            i=0
            max_x=0
            max_y=0
            j=0
            organizmy={}
            for line in text.readlines():
                i=0
                for word in line.split():
                    if (j==0 and i==1):
                        proba[i]=int(word)
                        max_x=proba[0]
                        max_y=proba[1]
                        self.__pochodzenie.setmaxX(max_x)
                        self.__pochodzenie.setmaxY(max_y)
                        self.__pochodzenie.setOrg()
                        self.__pochodzenie.setKol()
                        self.__pochodzenie.setSizeOfHeap(0)
                        organizmy=self.__pochodzenie.getOrg()
                    proba[i]=int(word)
                    i+=1
                if (j!=0):
                    if (proba[0] == komunikaty.Tabela_organizmow.TRAWA.value):
                        organizmy[proba[1] + (proba[2])*max_x] = trawa.Trawa(proba[1] , proba[2] , self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] == komunikaty.Tabela_organizmow.MLECZ.value):
                        organizmy[proba[1] + (proba[2])*max_x] = mlecz.Mlecz(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1]+ (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] == komunikaty.Tabela_organizmow.GUARANA.value):
                        organizmy[proba[1] + (proba[2])*max_x] = guarana.Guarana(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] == komunikaty.Tabela_organizmow.WILCZAJAGODA.value):
                        organizmy[proba[1] + (proba[2])*max_x] = wilczajagoda.WilczaJagoda(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] == komunikaty.Tabela_organizmow.WILK.value):
                        organizmy[proba[1] + (proba[2])*max_x] = wilk.Wilk(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] == komunikaty.Tabela_organizmow.OWCA.value):
                        organizmy[proba[1] + (proba[2])*max_x] = owca.Owca(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] == komunikaty.Tabela_organizmow.LIS.value):
                        organizmy[proba[1] + (proba[2])*max_x] = lis.Lis(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] == komunikaty.Tabela_organizmow.ZOLW.value):
                        organizmy[proba[1] + (proba[2])*max_x] = zolw.Zolw(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                        organizmy[proba[1] +  (proba[2])*max_x] = antylopa.Antylopa(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                    elif (proba[0] < 6):
                        organizmy[proba[1] + (proba[2])*max_x] = czlowiek.Czlowiek(proba[1], proba[2], self.__pochodzenie)
                        organizmy[proba[1] + (proba[2])*max_x].setSila(proba[3])
                        organizmy[proba[1] + (proba[2])*max_x].setWiek(proba[4])
                j+=1
            self.Komunikaty.clear()
            kom="WCZYTANO STAN GRY Z PLIKU "+nazwa
            self.Komunikaty.append(kom)
            for i in range(self.GameField.count()):
                self.GameField.itemAt(i).widget().close()
                self.GameField.itemAt(i).widget().deleteLater()
            self.__pochodzenie.setButtonsXY()
            QtGui.QGuiApplication.processEvents()
            self.zaladujNowaMape()
            self.WirutalnySwiat.grabKeyboard()
            self.WczytajGra.setVisible(False)
            self.__pochodzenie.setBlokada(False)
        except IOError:
            kom="NASTAPIL BLAD PRZY WCZYTYWANIU PLIKU "+nazwa+" POWOD: ZLA NAZWA PLIKU LUB USZKODZONY WCZYT"
            self.Komunikaty.append(kom)
            self.WirutalnySwiat.grabKeyboard()
            self.WczytajGra.setVisible(False)
            self.__pochodzenie.setBlokada(False)
        text.close()

    def stworzNowaGreAction(self):
        self.__pochodzenie.ui.WirutalnySwiat.releaseKeyboard()
        self.__pochodzenie.setBlokada(True)
        self.NowaGra.setVisible(True)

    def stworzNowaGre(self,intx,inty):
        for i in range(self.GameField.count()):
            self.GameField.itemAt(i).widget().close()
            self.GameField.itemAt(i).widget().deleteLater()
        self.__pochodzenie.setmaxX(intx)
        self.__pochodzenie.setmaxY(inty)
        self.__pochodzenie.setOrg()
        self.__pochodzenie.setKol()
        self.__pochodzenie.setSizeOfHeap(0)
        self.__pochodzenie.dodajOrganizmy()
   #     ButtonsXY=self.__pochodzenie.getButtonsXY()
        self.__pochodzenie.setButtonsXY()
        QtGui.QGuiApplication.processEvents()
        self.zaladujNowaMape()

    def tryNewOrganizm(self,x,y):
        self.__pochodzenie.ui.WirutalnySwiat.releaseKeyboard()
        self.__pochodzenie.setBlokada(True)
        self.uiNO.setX(x)
        self.uiNO.setY(y)
        self.OrganizmGra.setVisible(True)


    def zaladujNowaMape(self):
        import ButtonsMap
        max_x=self.__pochodzenie.getmaxX()
        max_y=self.__pochodzenie.getmaxY()
        organizmy=self.__pochodzenie.getOrg()
        ButtonsXY=self.__pochodzenie.getButtonsXY()
        for i in range(max_y):
            for j in range(max_x):
                # keep a reference to the buttons
                if (organizmy[j+max_x*i] is not None):
                    z=organizmy[j+max_x*i].rysowanie()
                    m=organizmy[j+max_x*i].getId()
                else :
                    z=' '
                    m=0
                ButtonsXY[(i, j)] = ButtonsMap.Button_X_Y(j,i,self.__pochodzenie)
                ButtonsXY[(i, j)].setText('%c' % (z))
                ButtonsXY[(i, j)].setObjectName('%c' % (z))
           #     ButtonsXY[(i, j)].clicked.connect(lambda: self.tryNewOrganizm(j,i))
            #    ButtonsXY[(i, j)].clicked.connect(lambda: self.tryNewOrganizm)
                if (m<10):
                    ButtonsXY[(i, j)].setStyleSheet("font-size:10px;color:blue;font-weight: bold;background-color:#C0C0C0  ;border: 0px solid #ffffff;")
                elif(m>=100 and m<=199):
                    ButtonsXY[(i, j)].setStyleSheet("font-size:10px;color:green;font-weight: bold;background-color:#C0C0C0  ;border: 0px solid #ffffff;")
                elif (m>=200 and m<=299):
                    ButtonsXY[(i, j)].setStyleSheet("font-size:10px;color:red;font-weight: bold;background-color:#C0C0C0 ;border: 0px solid #ffffff;")
                else:
                    ButtonsXY[(i, j)].setStyleSheet("font-size:10px;font-weight: bold;background-color:#C0C0C0 ;border: 0px solid #ffffff;")
                self.sizePolicy.setHorizontalStretch(0)
                self.sizePolicy.setVerticalStretch(0)
                self.sizePolicy.setHeightForWidth(ButtonsXY[(i, j)].sizePolicy().hasHeightForWidth())
                ButtonsXY[(i, j)].setSizePolicy(self.sizePolicy)
                # add to the layout
                self.GameField.addWidget(ButtonsXY[(i, j)], i, j)


    def zaladujMape(self):
        max_x=self.__pochodzenie.getmaxX()
        max_y=self.__pochodzenie.getmaxY()
        organizmy=self.__pochodzenie.getOrg()
        ButtonsXY=self.__pochodzenie.getButtonsXY()
        for i in range(max_y):
            for j in range(max_x):
                if (organizmy[j+max_x*i] is not None):
                    z=organizmy[j+max_x*i].rysowanie()
                    m=organizmy[j+max_x*i].getId()
                else :
                    z=' '
                    m=0
                ButtonsXY[(i, j)].setText('%c' % (z))
                if (m<10):
                    ButtonsXY[(i, j)].setStyleSheet("font-size:10px;color:blue;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
                elif(m>=100 and m<=199):
                    ButtonsXY[(i, j)].setStyleSheet("font-size:10px;color:green;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
                elif (m>=200 and m<=299):
                    ButtonsXY[(i, j)].setStyleSheet("font-size:10px;color:red;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
                else:
                    ButtonsXY[(i, j)].setStyleSheet("font-size:10px;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
        QtGui.QGuiApplication.processEvents()
