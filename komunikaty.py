import enum
class Komunikaty(enum.Enum):
    """description of class"""
    ZABIJ=9001
    UMIERAJ=9999
    ZUM=666
    ROZMNAZAJ=9003
    NIC=1111
    UCIEKL=707
    
class Tabela_organizmow(enum.Enum):
    """description of class"""
    TRAWA=100
    MLECZ=101
    GUARANA=102
    WILCZAJAGODA=103
    WILK=201
    OWCA=202
    LIS = 203
    ZOLW=204
    ANTYLOPA=205

