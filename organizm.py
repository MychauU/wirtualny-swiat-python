import abc
class Organizm(metaclass=abc.ABCMeta):
    """description of class"""

    def __init__(self,xx,yy,idd,silaa,inicjatywaa,wyraz,wiekk,pochod):
        self.__x=xx
        self.__y=yy
        self.__pochodzenie=pochod
        self.__id=idd
        self.__sila=silaa
        self.__inicjatywa=inicjatywaa
        self.__nazwa=wyraz
        self.__wiek=wiekk

    @abc.abstractmethod
    def akcja(self):
        pass
    @abc.abstractmethod
    def kolizja(self,kolizujacy):
        pass
    @abc.abstractmethod
    def rysowanie(self):
        pass

    def getNazwa(self):
        return self.__nazwa

    def setNazwa(self,x):
        self.__nazwa=x

    def getSila(self):
        return self.__sila

    def setSila(self, x):
        self.__sila=x

    def getInicjatywa(self):
        return self.__inicjatywa

    def setInicjatywa(self, x):
        self.__inicjatywa=x

    def getWiek(self):
        return self.__wiek;
    
    def setWiek(self, x):
        self.__wiek=x;
    
    def getId(self):
        return self.__id;
    
    def setId(self, x):
        self.__id=x;
    
    def getX(self):
        return self.__x;
    
    def setX(self, xx):
        self.__x=xx;
    
    def getY(self):
        return self.__y;
    
    def setY(self, x):
        self.__y=x;
    
    def getPochodzenie(self):
        return self.__pochodzenie

    def szukajwolnegomiejsca(self):
        pochodzenie=self.getPochodzenie()
        proba = 0;
        wynik=''
        xpom=self.getX()
        ypom=self.getY()
        max_x=pochodzenie.getmaxX()
        max_y=pochodzenie.getmaxY()
        organizmy = pochodzenie.getOrg()  
        if (xpom > 0 and ypom  > 0):
            if (organizmy[xpom - 1 + (ypom - 1)*max_x] is None):
                wynik+= '1'
                proba+=1
        if (ypom > 0):
            if ((organizmy[xpom + (ypom - 1)*max_x] is None)):
                wynik+= '2'
                proba+=1
        if (xpom<max_x-1 and ypom > 0):
            if ((organizmy[xpom + 1 + (ypom - 1)*max_x] is None)):
                wynik+= '3'
                proba+=1
        if (xpom> 0):
            if ((organizmy[xpom - 1 + (ypom)*max_x] is None)):
                wynik+= '4'
                proba+=1
        if (xpom < max_x -1):
            if ((organizmy[xpom + 1 + (ypom)*max_x] is None)):
                wynik+= '5'
                proba+=1
        if (xpom  >0 and ypom < max_y-1):
            if ((organizmy[xpom - 1 + (ypom + 1)*max_x] is None)):
                wynik+= '6'
                proba+=1
        if (ypom< max_y - 1):
            if ((organizmy[xpom + (ypom + 1)*max_x] is None)):
                wynik+= '7'
                proba+=1
        if (xpom < max_x - 1 and ypom  < max_y - 1):
            if ((organizmy[xpom +1+ (ypom + 1)*max_x] is None)):
                wynik+= '8'
                proba+=1
        if (proba == 0):
            return 0
        else:
            import random
            test = random.randint(0, proba-1)
            return wynik[test]

    def rozmnazaj(self,gdzie):
        import komunikaty
        import trawa
        import guarana
        import mlecz
        import wilczajagoda
        import wilk
        import owca
        import lis
        import antylopa
        import czlowiek
        import zolw
        TiD=self.getId()
        pochodzenie=self.getPochodzenie()
        xpom=self.getX()
        ypom=self.getY()
        max_x=pochodzenie.getmaxX()
        max_y=pochodzenie.getmaxY()
        organizmy = pochodzenie.getOrg()  
        if (gdzie=='1'):
            if (TiD == komunikaty.Tabela_organizmow.TRAWA.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = trawa.Trawa(xpom - 1, ypom - 1, pochodzenie)
            elif (TiD == komunikaty.Tabela_organizmow.MLECZ.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = mlecz.Mlecz(xpom - 1, ypom - 1, pochodzenie)
            elif (TiD == komunikaty.Tabela_organizmow.GUARANA.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = guarana.Guarana(xpom - 1, ypom - 1, pochodzenie)
            elif (TiD == komunikaty.Tabela_organizmow.WILCZAJAGODA.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = wilczajagoda.WilczaJagoda(xpom - 1, ypom - 1, pochodzenie)
            elif (TiD == komunikaty.Tabela_organizmow.WILK.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = wilk.Wilk(xpom - 1, ypom - 1, pochodzenie)
            elif (TiD == komunikaty.Tabela_organizmow.OWCA.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = owca.Owca(xpom - 1, ypom - 1, pochodzenie)
            elif (TiD == komunikaty.Tabela_organizmow.LIS.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = lis.Lis(xpom - 1, ypom - 1, pochodzenie)
            elif (TiD == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = antylopa.Antylopa(xpom - 1, ypom - 1, pochodzenie)
            elif (TiD == komunikaty.Tabela_organizmow.ZOLW.value):
                organizmy[xpom - 1 + (ypom - 1)*max_x] = zolw.Zolw(xpom - 1, ypom - 1, pochodzenie)
            if (TiD>=100 and TiD<=199):
                self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom - 1,ypom - 1,"R")
            elif (TiD>=200 and TiD<=299):
                self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom - 1,ypom - 1,"Z")

        elif (gdzie=='2'):
                if (TiD == komunikaty.Tabela_organizmow.TRAWA.value):
                    organizmy[xpom  + (ypom - 1)*max_x] = trawa.Trawa(xpom , ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.MLECZ.value):
                    organizmy[xpom + (ypom - 1)*max_x] = mlecz.Mlecz(xpom, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.GUARANA.value):
                    organizmy[xpom + (ypom - 1)*max_x] = guarana.Guarana(xpom, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILCZAJAGODA.value):
                    organizmy[xpom + (ypom - 1)*max_x] = wilczajagoda.WilczaJagoda(xpom, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILK.value):
                    organizmy[xpom + (ypom - 1)*max_x] = wilk.Wilk(xpom, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.OWCA.value):
                    organizmy[xpom + (ypom - 1)*max_x] = owca.Owca(xpom, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.LIS.value):
                    organizmy[xpom + (ypom - 1)*max_x] = lis.Lis(xpom, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ZOLW.value):
                    organizmy[xpom + (ypom - 1)*max_x] = zolw.Zolw(xpom, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                    organizmy[xpom + (ypom - 1)*max_x] = antylopa.Antylopa(xpom, ypom - 1, pochodzenie)
                if (TiD>=100 and TiD<=199):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom, ypom - 1,"R")
                elif (TiD>=200 and TiD<=299):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom, ypom - 1,"Z")

        elif (gdzie=='3'):
                if (TiD == komunikaty.Tabela_organizmow.TRAWA.value):
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = trawa.Trawa(xpom + 1, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.MLECZ.value):
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = mlecz.Mlecz(xpom + 1, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.GUARANA.value): 
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = guarana.Guarana(xpom + 1, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILCZAJAGODA.value): 
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = wilczajagoda.WilczaJagoda(xpom + 1, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILK.value):
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = wilk.Wilk(xpom + 1, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.OWCA.value):
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = owca.Owca(xpom + 1, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.LIS.value):
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = lis.Lis(xpom + 1, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ZOLW.value):
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = zolw.Zolw(xpom + 1, ypom - 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                    organizmy[xpom + 1 + (ypom - 1)*max_x] = antylopa.Antylopa(xpom + 1, ypom - 1, pochodzenie)
                if (TiD>=100 and TiD<=199):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom + 1, ypom ,"R")
                elif (TiD>=200 and TiD<=299):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom + 1, ypom ,"Z")

        elif (gdzie=='4'):
                if (TiD == komunikaty.Tabela_organizmow.TRAWA.value):
                    organizmy[xpom - 1 + (ypom)*max_x] = trawa.Trawa(xpom - 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.MLECZ.value): 
                    organizmy[xpom - 1 + (ypom)*max_x] = mlecz.Mlecz(xpom - 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.GUARANA.value): 
                    organizmy[xpom - 1 + (ypom)*max_x] = guarana.Guarana(xpom - 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILCZAJAGODA.value): 
                    organizmy[xpom - 1 + (ypom)*max_x] = wilczajagoda.WilczaJagoda(xpom - 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILK.value):
                    organizmy[xpom - 1 + (ypom)*max_x] = wilk.Wilk(xpom - 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.OWCA.value):
                    organizmy[xpom - 1 + (ypom)*max_x] = owca.Owca(xpom - 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.LIS.value):
                    organizmy[xpom - 1 + (ypom)*max_x] = lis.Lis(xpom - 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ZOLW.value):
                    organizmy[xpom - 1 + (ypom)*max_x] = zolw.Zolw(xpom - 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                    organizmy[xpom - 1 + (ypom)*max_x] = antylopa.Antylopa(xpom - 1, ypom, pochodzenie)
                if (TiD>=100 and TiD<=199):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom - 1, ypom ,"R")
                elif (TiD>=200 and TiD<=299):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom - 1, ypom ,"Z")

        elif (gdzie=='5'):
                if (TiD == komunikaty.Tabela_organizmow.TRAWA.value):
                    organizmy[xpom + 1 + (ypom)*max_x] = trawa.Trawa(xpom + 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.MLECZ.value): 
                    organizmy[xpom +1 + (ypom)*max_x] = mlecz.Mlecz(xpom + 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.GUARANA.value): 
                    organizmy[xpom + 1 + (ypom)*max_x] = guarana.Guarana(xpom + 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILCZAJAGODA.value): 
                    organizmy[xpom + 1 + (ypom)*max_x] = wilczajagoda.WilczaJagoda(xpom + 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILK.value):
                    organizmy[xpom + 1 + (ypom)*max_x] = wilk.Wilk(xpom + 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.OWCA.value):
                    organizmy[xpom + 1 + (ypom)*max_x] = owca.Owca(xpom + 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.LIS.value):
                    organizmy[xpom + 1 + (ypom)*max_x] = lis.Lis(xpom + 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ZOLW.value):
                    organizmy[xpom + 1 + (ypom)*max_x] = zolw.Zolw(xpom + 1, ypom, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                    organizmy[xpom + 1 + (ypom)*max_x] = antylopa.Antylopa(xpom + 1, ypom, pochodzenie)
                if (TiD>=100 and TiD<=199):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom + 1, ypom ,"R")
                elif (TiD>=200 and TiD<=299):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom + 1, ypom ,"Z")


        elif (gdzie=='6'):
                if (TiD == komunikaty.Tabela_organizmow.TRAWA.value):
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = trawa.Trawa(xpom - 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.MLECZ.value): 
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = mlecz.Mlecz(xpom - 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.GUARANA.value): 
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = guarana.Guarana(xpom - 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILCZAJAGODA.value): 
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = wilczajagoda.WilczaJagoda(xpom - 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILK.value):
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = wilk.Wilk(xpom - 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.OWCA.value):
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = owca.Owca(xpom - 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.LIS.value):
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = lis.Lis(xpom - 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ZOLW.value):
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = zolw.Zolw(xpom - 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                    organizmy[xpom - 1 + (ypom + 1)*max_x] = antylopa.Antylopa(xpom - 1, ypom + 1, pochodzenie)
                if (TiD>=100 and TiD<=199):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom - 1, ypom + 1 ,"R")
                elif (TiD>=200 and TiD<=299):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom - 1, ypom + 1 ,"Z")

        elif (gdzie=='7'):
                if (TiD == komunikaty.Tabela_organizmow.TRAWA.value):
                    organizmy[xpom + (ypom + 1)*max_x] = trawa.Trawa(xpom , ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.MLECZ.value): 
                    organizmy[xpom + (ypom + 1)*max_x] = mlecz.Mlecz(xpom, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.GUARANA.value): 
                    organizmy[xpom + (ypom + 1)*max_x] = guarana.Guarana(xpom, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILCZAJAGODA.value): 
                    organizmy[xpom + (ypom + 1)*max_x] = wilczajagoda.WilczaJagoda(xpom, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILK.value):
                    organizmy[xpom + (ypom + 1)*max_x] = wilk.Wilk(xpom, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.OWCA.value):
                    organizmy[xpom + (ypom + 1)*max_x] = owca.Owca(xpom, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.LIS.value):
                    organizmy[xpom + (ypom + 1)*max_x] = lis.Lis(xpom, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ZOLW.value):
                    organizmy[xpom + (ypom + 1)*max_x] = zolw.Zolw(xpom, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                    organizmy[xpom + (ypom + 1)*max_x] = antylopa.Antylopa(xpom, ypom + 1, pochodzenie)
                if (TiD>=100 and TiD<=199):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom , ypom + 1 ,"R")
                elif (TiD>=200 and TiD<=299):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom , ypom + 1 ,"Z")

        elif (gdzie=='8'):
                if (TiD == komunikaty.Tabela_organizmow.TRAWA.value):
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = trawa.Trawa(xpom + 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.MLECZ.value): 
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = mlecz.Mlecz(xpom + 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.GUARANA.value): 
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = guarana.Guarana(xpom + 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILCZAJAGODA.value): 
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = wilczajagoda.WilczaJagoda(xpom + 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.WILK.value):
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = wilk.Wilk(xpom + 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.OWCA.value):
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = owca.Owca(xpom + 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.LIS.value):
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = lis.Lis(xpom + 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ZOLW.value):
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = zolw.Zolw(xpom + 1, ypom + 1, pochodzenie)
                elif (TiD == komunikaty.Tabela_organizmow.ANTYLOPA.value):
                    organizmy[xpom + 1 + (ypom + 1)*max_x] = antylopa.Antylopa(xpom + 1, ypom + 1, pochodzenie)
                if (TiD>=100 and TiD<=199):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom + 1, ypom + 1 ,"R")
                elif (TiD>=200 and TiD<=299):
                    self.__pochodzenie.wypiszKomunikatRozmnazania(self.getNazwa(),xpom + 1, ypom + 1 ,"Z")