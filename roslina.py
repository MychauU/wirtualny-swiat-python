from organizm import Organizm
class Roslina(Organizm):
    """description of class"""
    def __init__(self,xx,yy, idd, silaa,inicjatywaa,wyraz,wiekk,pochod):
        super(Roslina,self).__init__(xx,yy,idd,silaa,inicjatywaa,wyraz,wiekk,pochod)

    def kolizja(self,kolizujacy):
        import komunikaty
        pochodzenie=self.getPochodzenie()
        pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"R",komunikaty.Komunikaty.ZABIJ.value)
        return komunikaty.Komunikaty.ZABIJ.value

    def akcja(self):
        import random
        proba = random.randint(0, 1000)
        if (proba >= 200 and proba <= 300):
            m=self.szukajwolnegomiejsca()
            if (m!=0):
                self.rozmnazaj(m)
                self.setWiek(self.getWiek()+1)
        self.setWiek(self.getWiek()+1)
