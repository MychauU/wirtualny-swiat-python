# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Informatyka\PycharmProjects\Wirtualnyswiat\SaveGameUI.ui'
#
# Created by: PyQt5 UI code generator 5.4.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_FormSave(QtWidgets.QWidget):
    def __init__(self,pochod):
        self.pochodzenie=pochod
        super(Ui_FormSave, self).__init__()

    def closeEvent(self, event):
        self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
        self.pochodzenie.setBlokada(False)

    def setupUi(self, FormSave):
        FormSave.setObjectName("FormSave")
        FormSave.resize(262, 163)
        FormSave.setMinimumSize(QtCore.QSize(262, 163))
        FormSave.setMaximumSize(QtCore.QSize(262, 163))
        self.zapiszButton = QtWidgets.QPushButton(FormSave)
        self.zapiszButton.setGeometry(QtCore.QRect(180, 130, 75, 23))
        self.zapiszButton.setObjectName("zapiszButton")

        self.zapiszButton.clicked.connect(self.trySaveGame)

        self.formLayoutWidget = QtWidgets.QWidget(FormSave)
        self.formLayoutWidget.setGeometry(QtCore.QRect(0, 20, 261, 101))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setFrameShape(QtWidgets.QFrame.Box)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.label)
        self.lineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit.setText("WS_przyklad.txt")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.lineEdit)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.formLayout.setItem(2, QtWidgets.QFormLayout.FieldRole, spacerItem)

        self.retranslateUi(FormSave)
        QtCore.QMetaObject.connectSlotsByName(FormSave)

    def retranslateUi(self, FormSave):
        _translate = QtCore.QCoreApplication.translate
        FormSave.setWindowTitle(_translate("FormSave", "Zapisz Gre"))
        self.zapiszButton.setText(_translate("FormSave", "Zapisz"))
        self.label.setText(_translate("FormSave", "Podaj nazwę dla zapisywanej gry:"))

    def trySaveGame(self):
        rawString=self.lineEdit.text()
        if (rawString != "" ):
            CzyString=self.isString(rawString)
            if (CzyString==True):
                nazwa=str(rawString)
                self.pochodzenie.ui.zapiszGre(nazwa)
                KomunikatBox=self.pochodzenie.ui.getKomunikaty()
                kom="ZAPISALES GRE POD NAZWA "+nazwa
                self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
                self.pochodzenie.ui.ZapiszGra.setVisible(False)
                self.pochodzenie.setBlokada(False)
                KomunikatBox.append(kom)
            else:
                KomunikatBox=self.pochodzenie.ui.getKomunikaty()
                kom="NAZWA PLIKU  NIE JEST POPRAWNA LEKSYKOGRAFICZNIE"
                KomunikatBox.append(kom)
        else:
            KomunikatBox=self.pochodzenie.ui.getKomunikaty()
            kom="POLE NAZWA PLIKU NIE JEST UZUPELNIONE"
            KomunikatBox.append(kom)

    @staticmethod
    def isString(x):
        try:
            str(x)
            return True
        except ValueError:
            return False

