# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Informatyka\PycharmProjects\Wirtualnyswiat\NewOrganizm.ui'
#
# Created by: PyQt5 UI code generator 5.4.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_NewOrganizmForm(QtWidgets.QWidget):
    def __init__(self,pochod):
        self.pochodzenie=pochod
        super(Ui_NewOrganizmForm, self).__init__()
        self.x=0
        self.y=0

    def setX(self,xx):
        self.x=xx

    def setY(self,yy):
        self.y=yy

    def closeEvent(self, event):
        self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
        self.pochodzenie.setBlokada(False)

    def setupUi(self, NewOrganizmForm):
        NewOrganizmForm.setObjectName("NewOrganizmForm")
        NewOrganizmForm.resize(267, 326)
        self.formLayoutWidget = QtWidgets.QWidget(NewOrganizmForm)
        self.formLayoutWidget.setGeometry(QtCore.QRect(0, 10, 261, 311))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setFrameShape(QtWidgets.QFrame.Box)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.label)
        self.trawaButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.trawaButton.setObjectName("trawaButton")

        self.trawaButton.clicked.connect(self.tryNewTrawa)

        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.trawaButton)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.formLayout.setItem(1, QtWidgets.QFormLayout.FieldRole, spacerItem)
        self.mleczButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.mleczButton.setObjectName("mleczButton")

        self.mleczButton.clicked.connect(self.tryNewMlecz)

        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.mleczButton)
        self.guaranaButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.guaranaButton.setObjectName("guaranaButton")

        self.guaranaButton.clicked.connect(self.tryNewGuarana)

        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.guaranaButton)
        self.wilczajagodaButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.wilczajagodaButton.setObjectName("wilczajagodaButton")

        self.wilczajagodaButton.clicked.connect(self.tryNewWilczaJagoda)

        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.wilczajagodaButton)
        self.wilkButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.wilkButton.setObjectName("wilkButton")

        self.wilkButton.clicked.connect(self.tryNewWilk)

        self.formLayout.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.wilkButton)
        self.lisButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.lisButton.setObjectName("lisButton")

        self.lisButton.clicked.connect(self.tryNewLis)

        self.formLayout.setWidget(7, QtWidgets.QFormLayout.FieldRole, self.lisButton)
        self.owcaButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.owcaButton.setObjectName("owcaButton")

        self.owcaButton.clicked.connect(self.tryNewOwca)

        self.formLayout.setWidget(8, QtWidgets.QFormLayout.FieldRole, self.owcaButton)
        self.zolwButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.zolwButton.setObjectName("zolwButton")

        self.zolwButton.clicked.connect(self.tryNewZolw)

        self.formLayout.setWidget(9, QtWidgets.QFormLayout.FieldRole, self.zolwButton)
        self.antylopaButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.antylopaButton.setObjectName("antylopaButton")

        self.antylopaButton.clicked.connect(self.tryNewAntylopa)

        self.formLayout.setWidget(10, QtWidgets.QFormLayout.FieldRole, self.antylopaButton)

        self.retranslateUi(NewOrganizmForm)
        QtCore.QMetaObject.connectSlotsByName(NewOrganizmForm)

    def retranslateUi(self, NewOrganizmForm):
        _translate = QtCore.QCoreApplication.translate
        NewOrganizmForm.setWindowTitle(_translate("NewOrganizmForm", "Dodaj Organizm"))
        self.label.setText(_translate("NewOrganizmForm", "Nacisnij dany przycisk aby stworzyc organizm"))
        self.trawaButton.setText(_translate("NewOrganizmForm", "Trawa"))
        self.mleczButton.setText(_translate("NewOrganizmForm", "Mlecz"))
        self.guaranaButton.setText(_translate("NewOrganizmForm", "Guarana"))
        self.wilczajagodaButton.setText(_translate("NewOrganizmForm", "Wilcza Jagoda"))
        self.wilkButton.setText(_translate("NewOrganizmForm", "Wilk"))
        self.lisButton.setText(_translate("NewOrganizmForm", "Lis"))
        self.owcaButton.setText(_translate("NewOrganizmForm", "Owca"))
        self.zolwButton.setText(_translate("NewOrganizmForm", "Zolw"))
        self.antylopaButton.setText(_translate("NewOrganizmForm", "Antylopa"))

    def tryNewTrawa(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import trawa
            organizmy[self.x+self.y*max_x]=trawa.Trawa(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:green;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Trawa na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)

    def tryNewMlecz(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import mlecz
            organizmy[self.x+self.y*max_x]=mlecz.Mlecz(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:green;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Mlecz na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)

    def tryNewGuarana(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import guarana
            organizmy[self.x+self.y*max_x]=guarana.Guarana(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:green;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Guarana na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)

    def tryNewWilczaJagoda(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import wilczajagoda
            organizmy[self.x+self.y*max_x]=wilczajagoda.WilczaJagoda(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:green;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Wilcza Jagoda na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)

    def tryNewWilk(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import wilk
            organizmy[self.x+self.y*max_x]=wilk.Wilk(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:red;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Wilk na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)

    def tryNewLis(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import lis
            organizmy[self.x+self.y*max_x]=lis.Lis(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:red;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Lis na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)

    def tryNewOwca(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import owca
            organizmy[self.x+self.y*max_x]=owca.Owca(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:red;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Owca na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)

    def tryNewZolw(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import zolw
            organizmy[self.x+self.y*max_x]=zolw.Zolw(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:red;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Zolw na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)

    def tryNewAntylopa(self):
        organizmy=self.pochodzenie.getOrg()
        max_x=self.pochodzenie.getmaxX()
        KomunikatBox=self.pochodzenie.ui.getKomunikaty()
        if (organizmy[self.x+self.y*max_x] is None):
            import antylopa
            organizmy[self.x+self.y*max_x]=antylopa.Antylopa(self.x,self.y,self.pochodzenie)
            ButtonsXY=self.pochodzenie.getButtonsXY()
            z=organizmy[self.x+self.y*max_x].rysowanie()
            ButtonsXY[(self.y,self.x)].setStyleSheet("font-size:10px;color:red;font-weight: bold;background-color:#C0C0C0;border: 0px solid #ffffff;")
            ButtonsXY[(self.y,self.x)].setText('%c' % (z))
            self.pochodzenie.ui.WirutalnySwiat.grabKeyboard()
            self.pochodzenie.ui.OrganizmGra.setVisible(False)
            self.pochodzenie.setBlokada(False)
            kom="Stworzyles Antylopa na pozycji "+str(self.x) +" "+str(self.y)
            KomunikatBox.append(kom)
            QtGui.QGuiApplication.processEvents()
        else:
            kom="Nie mozesz nic stworzyc, pozycja "+str(self.x)+" "+str(self.y)+" jest juz zajeta"
            KomunikatBox.append(kom)