from zwierze import Zwierze
class Antylopa(Zwierze):
    """description of class"""
    def __init__(self,xx,yy,pochod):
        super(Antylopa,self).__init__(xx,yy,205,4,4,"Antylopa",1,pochod)

    def rysowanie(self):
        return 'A'

    def kolizja(self,kolizujacy):
        import komunikaty
        if (kolizujacy.getId() == self.getId()):
            if (kolizujacy.getZmeczony() == 0 and self.getZmeczony() == 0):
                self.setZmeczony(6)
                return komunikaty.Komunikaty.ROZMNAZAJ.value
            else:
                return komunikaty.Komunikaty.NIC.value

        elif (kolizujacy.getSila() >= self.getSila()):
            import random
            pom =random.randint(0,1)
            if (pom==1):
                pochodzenie=self.getPochodzenie()
                pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"Z",komunikaty.Komunikaty.ZABIJ.value)
                return komunikaty.Komunikaty.ZABIJ.value
            else:                
                m=self.szukajwolnegomiejsca()
                if (m!=0):
                    xpom=self.getX()
                    ypom=self.getY()
                    pochodzenie=self.getPochodzenie()
                    max_x = pochodzenie.getmaxX()
                    max_y = pochodzenie.getmaxY()
                    organizmy = pochodzenie.getOrg()
                    if (m==1):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom - 1)*max_x)
                        self.setY(ypom-1)
                        self.setX(xpom-1)
                    elif(m==2):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x)
                        self.setY(ypom-1)
                    elif(m==3):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom - 1)*max_x)
                        self.setY(ypom-1)
                        self.setX(xpom+1)
                    elif(m==4):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x)
                        self.setX(xpom-1)
                    elif(m==5):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x)
                        self.setX(xpom+1)
                    elif(m==6):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom + 1)*max_x)
                        self.setX(xpom-1)
                        self.setY(ypom+1)
                    elif(m==7):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x)
                        self.setY(ypom+1)
                    elif(m==8):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom + 1)*max_x)
                        self.setX(xpom+1)
                        self.setY(ypom+1)
                    pochodzenie=self.getPochodzenie()
                    pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"Z",komunikaty.Komunikaty.UCIEKL.value)
                    return komunikaty.Komunikaty.UCIEKL.value
                else:
                    pochodzenie=self.getPochodzenie()
                    pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"Z",komunikaty.Komunikaty.ZABIJ.value)
                    return komunikaty.Komunikaty.ZABIJ.value

        elif (kolizujacy.getSila() < self.getSila()):
            pochodzenie=self.getPochodzenie()
            pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"Z",komunikaty.Komunikaty.UMIERAJ.value)
            return komunikaty.Komunikaty.UMIERAJ.value
        else:
            return komunikaty.Komunikaty.NIC.value


    def akcja(self):
        import random
        pochodzenie=self.getPochodzenie()
        max_x = pochodzenie.getmaxX()
        max_y = pochodzenie.getmaxY()
        organizmy = pochodzenie.getOrg()
        size_active = pochodzenie.getSizeOfHeap()
        kolejnosc = pochodzenie.getKol()
        ruch=random.randint(0, 3)
        xpom=self.getX()
        ypom=self.getY()
        import swiat
        import komunikaty
        if (ruch==0):
            if (ypom > 1):
                if ((organizmy[xpom + (ypom - 2)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 2)*max_x)
                    self.setY(ypom-2)
                else:
                    zdarzenie = organizmy[xpom + (ypom - 2)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom - 2)*max_x)
                        self.setY(ypom-2)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + (ypom - 2)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
                #                del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + (ypom - 2)*max_x] = None
                 #           del organizmy[xpom + (ypom - 2)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom - 2)*max_x);
                        self.setY(ypom-2);

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
    #                    del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + (ypom - 2)*max_x] == kolejnosc[i]):
                     #           del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + (ypom - 2)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
              #              del organizmy[xpom + (ypom - 2)*max_x]
            #                del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (ruch==1):
            if (ypom <max_y-2):
                if ((organizmy[xpom + (ypom + 2)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 2)*max_x)
                    self.setY(ypom+2)
                else:
                    zdarzenie = organizmy[xpom + (ypom + 2)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom + 2)*max_x)
                        self.setY(ypom+2)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + (ypom + 2)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
           #                     del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + (ypom + 2)*max_x] = None
                #            del organizmy[xpom + (ypom + 2)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom + 2)*max_x)
                        self.setY(ypom+2)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
             #           del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + (ypom + 2)*max_x] == kolejnosc[i]):
           #                     del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + (ypom + 2)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
            #                del organizmy[xpom + (ypom + 2)*max_x]
           #                 del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (ruch==2):
            if (xpom >1):
                if ((organizmy[xpom - 2 + (ypom)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 2 + (ypom)*max_x)
                    self.setX(xpom-2)
                else:
                    zdarzenie = organizmy[xpom - 2 + (ypom)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom - 2 + (ypom)*max_x)
                        self.setX(xpom-2)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom - 2 + (ypom)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
                #                del kolejnosc[i]
                                break
                                              
                        organizmy[xpom - 2 + (ypom)*max_x] = None;
               #             del organizmy[xpom - 2 + (ypom)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom - 2 + (ypom)*max_x);
                        self.setX(xpom-2)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
            #            del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom - 2 + (ypom)*max_x] == kolejnosc[i]):
                 #               del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom - 2 + (ypom)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
             #               del organizmy[xpom - 2 + (ypom)*max_x]
            #                del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)

        elif (ruch==3):
            if (xpom <max_x-2):
                if ((organizmy[xpom + 2 + (ypom)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 2 + (ypom)*max_x)
                    self.setX(xpom+2)
                else:
                    zdarzenie = organizmy[xpom + 2 + (ypom)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + 2 + (ypom)*max_x)
                        self.setX(xpom+2)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + 2 + (ypom)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
          #                      del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + 2 + (ypom)*max_x] = None
                    #        del organizmy[xpom + 2 + (ypom)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + 2 + (ypom)*max_x);
                        self.setX(xpom+2)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
             #           del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + 2 + (ypom)*max_x] == kolejnosc[i]):
                 #               del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + 2 + (ypom)*max_x] = None;
                        organizmy[xpom + (ypom)*max_x] = None;
             #               del organizmy[xpom + 2 + (ypom)*max_x]
             #               del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.setZmeczony(10)
        if (self.getZmeczony()>0):
            self.setZmeczony(self.getZmeczony()-1)
        self.setWiek(self.getWiek()+1)           