from zwierze import Zwierze
class Zolw(Zwierze):
    """description of class"""
    def __init__(self,xx,yy,pochod):
        super(Zolw,self).__init__(xx,yy,204,2,1,"Zolw",1,pochod)

    def rysowanie(self):
        return 'T'

    def akcja(self):
        import random
        pom=random.randint(0, 3)
        if (pom == 0):
            super(Zolw,self).akcja()
        else:
            self.setWiek(self.getWiek()+1)
            zmeczony=self.getZmeczony()
            if (zmeczony > 0):
                self.setZmeczony(zmeczony-1)

    def kolizja(self,kolizujacy):
        import komunikaty
        if (kolizujacy.getId() == self.getId()):
            if (kolizujacy.getZmeczony() == 0 and self.getZmeczony() == 0):
                self.setZmeczony(6);
                return komunikaty.Komunikaty.ROZMNAZAJ.value
            else:
                return komunikaty.Komunikaty.NIC.value
        elif (kolizujacy.getSila()-5 >= self.getSila()):
            pochodzenie=self.getPochodzenie()
            pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"Z",komunikaty.Komunikaty.ZABIJ.value)
            return komunikaty.Komunikaty.ZABIJ.value
        elif (kolizujacy.getSila() < self.getSila()):
            pochodzenie=self.getPochodzenie()
            pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"Z",komunikaty.Komunikaty.ZABIJ.value)
            return komunikaty.Komunikaty.UMIERAJ.value
        else:
            pochodzenie=self.getPochodzenie()
            KomunikatBox=pochodzenie.ui.getKomunikaty()
            kom=kolizujacy.getNazwa()+" na pozycji "+str(kolizujacy.getX())+ " "+str(kolizujacy.getY())+" atakuje Zolw na pozycji "+str(self.getX())+ " "+str(self.getY())+" ale Zolw obronil sie"
            KomunikatBox.append(kom)
            return komunikaty.Komunikaty.NIC.value  