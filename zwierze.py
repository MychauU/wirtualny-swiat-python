from organizm import Organizm
class Zwierze(Organizm):
    """description of class"""
    def __init__(self,xx,yy, idd, silaa,inicjatywaa,wyraz,wiekk,pochod):
        self.__zmeczony=0
        super(Zwierze,self).__init__(xx,yy,idd,silaa,inicjatywaa,wyraz,wiekk,pochod)

    def addSila(self,i):
        self.setSila(self.getSila()+i)

    def getZmeczony(self):
        return self.__zmeczony

    def setZmeczony(self,zmeczon):
        self.__zmeczony=zmeczon

    def akcja(self):
        import random
        pochodzenie=self.getPochodzenie()
        max_x = pochodzenie.getmaxX()
        max_y = pochodzenie.getmaxY()
        organizmy = pochodzenie.getOrg()
        size_active = pochodzenie.getSizeOfHeap()
        kolejnosc = pochodzenie.getKol()
        ruch=random.randint(0, 3)
        xpom=self.getX()
        ypom=self.getY()
        import swiat
        import komunikaty
        if (ruch==0):
            if (ypom > 0):
                if ((organizmy[xpom + (ypom - 1)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x)
                    self.setY(ypom-1)
                else:
                    zdarzenie = organizmy[xpom + (ypom - 1)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom - 1)*max_x)
                        self.setY(ypom-1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
                #                del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + (ypom - 1)*max_x] = None
                 #           del organizmy[xpom + (ypom - 1)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom - 1)*max_x);
                        self.setY(ypom-1);

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
    #                    del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]):
                     #           del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + (ypom - 1)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
              #              del organizmy[xpom + (ypom - 1)*max_x]
            #                del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.__zmeczony=10

        elif (ruch==1):
            if (ypom <max_y-1):
                if ((organizmy[xpom + (ypom + 1)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x)
                    self.setY(ypom+1)
                else:
                    zdarzenie = organizmy[xpom + (ypom + 1)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom + 1)*max_x)
                        self.setY(ypom+1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
           #                     del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + (ypom + 1)*max_x] = None
                #            del organizmy[xpom + (ypom + 1)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + (ypom + 1)*max_x)
                        self.setY(ypom+1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
             #           del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]):
           #                     del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + (ypom + 1)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
            #                del organizmy[xpom + (ypom + 1)*max_x]
           #                 del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.__zmeczony=10

        elif (ruch==2):
            if (xpom >0):
                if ((organizmy[xpom - 1 + (ypom)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x)
                    self.setX(xpom-1)
                else:
                    zdarzenie = organizmy[xpom - 1 + (ypom)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom - 1 + (ypom)*max_x)
                        self.setX(xpom-1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom - 1 + (ypom)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
                #                del kolejnosc[i]
                                break
                                              
                        organizmy[xpom - 1 + (ypom)*max_x] = None;
               #             del organizmy[xpom - 1 + (ypom)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom - 1 + (ypom)*max_x);
                        self.setX(xpom-1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
            #            del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom - 1 + (ypom)*max_x] == kolejnosc[i]):
                 #               del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom - 1 + (ypom)*max_x] = None
                        organizmy[xpom + (ypom)*max_x] = None
             #               del organizmy[xpom - 1 + (ypom)*max_x]
            #                del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.__zmeczony=10

        elif (ruch==3):
            if (xpom <max_x-1):
                if ((organizmy[xpom + 1 + (ypom)*max_x] == None)):
                    pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x)
                    self.setX(xpom+1)
                else:
                    zdarzenie = organizmy[xpom + 1 + (ypom)*max_x].kolizja(self);
                    if (zdarzenie == komunikaty.Komunikaty.UCIEKL.value):
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + 1 + (ypom)*max_x)
                        self.setX(xpom+1)
                    elif (zdarzenie == komunikaty.Komunikaty.ZABIJ.value):
                        for i in range(0, size_active):
                            if (organizmy[xpom + 1 + (ypom)*max_x] == kolejnosc[i]):
                                kolejnosc[i]=None
          #                      del kolejnosc[i]
                                break
                                              
                        organizmy[xpom + 1 + (ypom)*max_x] = None
                    #        del organizmy[xpom + 1 + (ypom)*max_x]
                        pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x, xpom + 1 + (ypom)*max_x);
                        self.setX(xpom+1)

                    elif (zdarzenie == komunikaty.Komunikaty.UMIERAJ.value):
             #           del organizmy[xpom + (ypom)*max_x]
                        organizmy[xpom + (ypom)*max_x] = None;
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.ZUM.value):
                        for i in range(0,size_active):
                            if (organizmy[xpom + 1 + (ypom)*max_x] == kolejnosc[i]):
                 #               del kolejnosc[i]
                                kolejnosc[i] = None
                                break
                        organizmy[xpom + 1 + (ypom)*max_x] = None;
                        organizmy[xpom + (ypom)*max_x] = None;
             #               del organizmy[xpom + 1 + (ypom)*max_x]
             #               del organizmy[xpom + (ypom)*max_x]
                        return

                    elif (zdarzenie == komunikaty.Komunikaty.NIC.value):
                        pass
                    elif (zdarzenie == komunikaty.Komunikaty.ROZMNAZAJ.value):
                        m=self.szukajwolnegomiejsca()
                        if (m!=0):
                            self.rozmnazaj(m)
                            self.__zmeczony=10
        if (self.__zmeczony>0):
            self.__zmeczony-=1
        self.setWiek(self.getWiek()+1)



    def kolizja(self,kolizujacy):
        import komunikaty
        if (kolizujacy.getId() == self.getId()):
            if (kolizujacy.getZmeczony() == 0 and self.__zmeczony == 0):
                self.__zmeczony = 6
                return komunikaty.Komunikaty.ROZMNAZAJ.value
            else:
                return komunikaty.Komunikaty.NIC.value
        elif (kolizujacy.getSila() >= self.getSila()):
            pochodzenie=self.getPochodzenie()
            pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"Z",komunikaty.Komunikaty.ZABIJ.value)
            return komunikaty.Komunikaty.ZABIJ.value
        elif (kolizujacy.getSila() < self.getSila()):
            pochodzenie=self.getPochodzenie()
            pochodzenie.wypiszKomunikatWalki(kolizujacy.getNazwa(),kolizujacy.getX(),kolizujacy.getY(),self.getNazwa(),self.getX(),self.getY(),"Z",komunikaty.Komunikaty.UMIERAJ.value)
            return komunikaty.Komunikaty.UMIERAJ.value
        else:
            return komunikaty.Komunikaty.NIC.value